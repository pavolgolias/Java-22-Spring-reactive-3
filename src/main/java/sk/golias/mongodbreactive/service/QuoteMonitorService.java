package sk.golias.mongodbreactive.service;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import sk.golias.mongodbreactive.client.StockQuoteClient;
import sk.golias.mongodbreactive.domain.Quote;
import sk.golias.mongodbreactive.repositories.QuoteRepository;

@Service
public class QuoteMonitorService implements ApplicationListener<ContextRefreshedEvent> {
    private final StockQuoteClient stockQuoteClient;
    private final QuoteRepository quoteRepository;

    public QuoteMonitorService(StockQuoteClient stockQuoteClient, QuoteRepository quoteRepository) {
        this.stockQuoteClient = stockQuoteClient;
        this.quoteRepository = quoteRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        stockQuoteClient.getQuoteStream()
            .log("quote-monitor-service")
            .subscribe(quote -> {
                Mono<Quote> savedQuote = quoteRepository.save(quote);

                System.out.println("I saved a quote! Id: " + savedQuote.block().getId());
                //System.out.println("I saved a quote!" + savedQuote.block());
            }, error -> {
                System.out.println(error);
            });
    }
}
