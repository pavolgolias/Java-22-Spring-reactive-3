package sk.golias.mongodbreactive;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import reactor.core.Disposable;
import reactor.core.publisher.Flux;
import sk.golias.mongodbreactive.client.StockQuoteClient;
import sk.golias.mongodbreactive.domain.Quote;
import sk.golias.mongodbreactive.repositories.QuoteRepository;

@Component
public class QuoteRunner implements CommandLineRunner {
    private final StockQuoteClient stockQuoteClient;
    private final QuoteRepository quoteRepository;

    public QuoteRunner(StockQuoteClient stockQuoteClient, QuoteRepository quoteRepository) {
        this.stockQuoteClient = stockQuoteClient;
        this.quoteRepository = quoteRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        // EXAMPLE 1:
        // Flux<Quote> quoteFlux = stockQuoteClient.getQuoteStream();
        // quoteFlux.subscribe(System.out::println);

        Flux<Quote> quoteFlux = quoteRepository.findWithTailableCursorBy();
        Disposable disposable = quoteFlux.subscribe(quote -> {
            System.out.println("*#*#*#*#*#*#*#*#*#*#*#*#* Id: " + quote.getId());
        });

        disposable.dispose();
    }
}
